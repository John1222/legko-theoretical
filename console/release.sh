#!/bin/bash

sudo docker exec -it www_nodejs_1 npm run build:prod
rsync -azvv "$(dirname $(readlink -f $0))/../development/public" "$(dirname $(readlink -f $0))/../production"