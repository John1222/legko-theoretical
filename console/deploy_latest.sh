#!/bin/bash

# Just kill firewalls
sudo ufw disable
service iptables stop
service ipchains stop
chkconfig ipchains off
chkconfig iptables off

# Docker
sudo apt-get update
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
sudo apt-get update
sudo apt-get install docker-engine -y
sudo apt-get install docker-compose -y
sudo docker network create external-net

# Repo
sudo mkdir -m 777 /var/www && cd /var/www
sudo apt-get install git -y
sudo git clone https://brocoder1@bitbucket.org/brocoder_team/test-job-2.git .
chmod -R +x ./console

# Docker project
sudo docker rm -f $(docker ps -a -q)
sudo docker-compose up -d