const merge = require( 'webpack-merge' );
const commonConfig = require( './common.config.js' );
var path = require( "path" );

module.exports = merge( commonConfig, {
    mode: 'production',
    output: {
        path: path.resolve( __dirname, './../production/public/dist' ),
        publicPath: 'dist/'
    },
} );