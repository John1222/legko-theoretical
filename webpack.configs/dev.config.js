const merge = require( 'webpack-merge' );
const commonConfig = require( './common.config.js' );
var path = require( "path" );

module.exports = merge( commonConfig, {
    mode: 'development',
    devtool: 'inline-source-map',
    output: {
        path: path.resolve( __dirname, './../development/public/dist' ),
        publicPath: 'dist/'
    },
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        contentBase: path.join( __dirname, './../development/public' ),
    }
} );